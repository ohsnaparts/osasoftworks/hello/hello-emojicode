# Hello-Emojicode

An [Emojicode](https://www.emojicode.org/) playground.

## Emojicode

Emojicode is an open-source, full-blown programming language consisting of emojis.

* [Getting Started](https://www.emojicode.org/docs/guides/compile-and-run.html)
* [Language Reference](https://www.emojicode.org/docs/reference/)


![](./img/hello-emojicode.jpg)

## Run

```bash
emojicodec ./hello-emojicode.emojic
./hello-emojicode <args>
```

## Development

I used [Visual Studio Code](https://code.visualstudio.com/) with the [:emojisense:](https://marketplace.visualstudio.com/items?itemName=bierner.emojisense) Extension.
